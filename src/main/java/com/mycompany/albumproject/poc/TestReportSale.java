/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.albumproject.poc;

import com.mycompany.albumproject.model.ReportSale;
import com.mycompany.albumproject.service.ReportService;
import java.util.List;

/**
 *
 * @author kitti
 */
public class TestReportSale {

    public static void main(String[] args) {
        ReportService reportService = new ReportService();

        //Dayre port
//        List<ReportSale> dayReport = reportService.getReportSaleByDay();
//        for (ReportSale reportSale : dayReport) {
//            System.out.println(reportSale);
//        }
        //Month report
        List<ReportSale> dayReport = reportService.getReportSaleByYear(2013);
        for (ReportSale reportSale : dayReport) {
            System.out.println(reportSale);
        }
    }
}
