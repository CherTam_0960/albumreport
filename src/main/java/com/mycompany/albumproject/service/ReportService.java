/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.albumproject.service;

import com.mycompany.albumproject.dao.ReportSaleDao;
import com.mycompany.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author kitti
 */
public class ReportService {

    public List<ReportSale> getReportSaleByDay() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getDayReport();
    }

    public List<ReportSale> getReportSaleByMonth() {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getMonthReport();
    }

    public List<ReportSale> getReportSaleByYear(int year) {
        ReportSaleDao dao = new ReportSaleDao();
        return dao.getMonthReport(year);
    }
}
